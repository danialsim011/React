import React, { useState } from "react";

import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
	const [isEditingMode, setIsEditingMode] = useState(false);

	const saveExpenseDataHandler = (enteredExpenseData) => {
		const expenseData = {
			...enteredExpenseData,
			id: Math.random().toString(),
		};
		props.onAddExpense(expenseData);
		setIsEditingMode(false);
	};

	const startEditingModeHandler = () => {
		setIsEditingMode(true);
	};

	const stopEditingModeHandler = () => {
		setIsEditingMode(false);
	};

	return (
		<div className="new-expense">
			{!isEditingMode && <button onClick={startEditingModeHandler}>Add New Expense</button>}
			{isEditingMode && (
				<ExpenseForm
					onSaveExpenseData={saveExpenseDataHandler}
					onCancel={stopEditingModeHandler}
				/>
			)}
		</div>
	);
};

export default NewExpense;
