# React
This folder is all about React. All projects were generated with [React](https://github.com/facebook/react).
All projects here have been built under React 17.0.2 in July 2021.

## :pushpin: React Project 01 (react-project01)
React Basics & Working with Components + React State & Working with Events + Rendering Lists & Conditional Content and assignments 1 to 4

### Content
1. React Basics
1. Components
1. Props
1. JSX (Extension of JavaScript)
1. Working with State
1. Working with Events
1. Two-way binding
1. Stateful Lists
1. Understanding `keys`
1. Conditional Content and Dynamic Styles

Inclusive of:
1. Assignment 1 (React & Component Basics)
1. Assignment 2 (Working with Events & State)
1. Assignment 3 (Working with Lists)
1. Assignment 4 (Conditional Content)

## :pushpin: React Project 02 (react-project02)
Styling React Components

### Content
1. Dynamic Inline Styles
1. Dynamic CSS Class
1. Styled Components
1. CSS Modules

## :pushpin: React Project 03 (react-project03)
Practice Project: User Management + Deep Dive: Working with Fragments, Portals and `Refs`

### Content
1. Recap on components
1. Recap on state management
1. Recap on working with lists
1. Adding error modal and manage error state
1. Wrapper Component
1. React Fragments
1. React Portals
1. `ref`s

## :pushpin: React Project 04 (react-project04)
Handling Side Effects, Using Reducers & Using the Context API

### Content
1. `useEffect()` Hook
1. `useReducer()` Hook
1. `useState()` Hook
1. React Context API
1. Forward Refs

## :pushpin: React Project 05 (react-project05)
Practice Project: Food Order App

### Content
1. Revision on Basics
1. Revision on React Portal and States
1. Revision on Hooks
1. Revision on Context
1. Revision on Refs
1. Revision on HTTP Requests
1. Revision on Forms Handling

## :pushpin: React Project 06 (react-project06)
Sending HTTP Requests (e.g. Connecting to a Database)

### Content
1. HTTP Requests (GET and POST)
1. `async` and `await`
1. Integration with state management and hooks

## :pushpin: React Project 07A (react-project07a)
Building Custom React Hooks

### Content
1. Basics of custom hooks

## :pushpin: React Project 07B (react-project07b)
Building Custom React Hooks (with a More Realistic Example)

### Content
1. Custom HTTP hook

## :pushpin: React Project 08 (react-project08)
Working with Forms & User Input

### Content
1. Form Submission and Validation
1. Form State (touched, lost focus)
1. Apply custom hook

## :pushpin: React Project 09 (react-project09)
Diving into Redux

### Content
1. Redux
1. Redux Toolkit

## :pushpin: React Project 10 (react-project10)
Advanced Redux

### Content
1. `useEffect` with Redux
1. Action Creator Thunk

## :pushpin: React Project 11A (react-project11a)
Multi-Page Single Page Application (SPA)

### Content
1. Basic Routing
1. React Router
1. Dynamic Routes
1. `switch` and `exact` to configure routes
1. Nested Routes

## :pushpin: React Project 11B (react-project11b)
Multi-Page Single Page Application (SPA) - More Realistic Project

### Content
1. Practice with Route Basics and Concepts
1. Query Parameters
1. Flexible Routing
1. Integration with HTTP Requests

## :pushpin: React Project 12 (react-project12)
Adding Authentication to React Apps

### Content
1. Authentication State Management
1. Authentication Processes

## :pushpin: React Project 13 (react-project13)
NextJS (Pretty Deep Dive)

## :pushpin: React Bonus Project 01 (react-bonus-project01)
React Animations

## :pushpin: React Bonus Project 02 (react-bonus-project02)
Replacing Redux with React Hooks

## :pushpin: React Bonus Project 03 (react-bonus-project03)
Testing React Apps (Unit Tests)

## :pushpin: React Bonus Project 04 (react-bonus-project04)
React + TypeScript

## :pushpin: React Optional Project 01 (react-optional-project01)
A Look Behind the Scenes of React & Optimisation Techniques

## :pushpin: React Optional Project 02 (react-optional-project02)
Class-based Components

## :pushpin: React Optional Project 03 (react-optional-project03)
React Hooks Refresher

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
